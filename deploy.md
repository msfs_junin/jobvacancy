# Despliegue de la Aplicación JobVacancy en Heroku

En este documento agrego informacion relevante para hacer el despliegue de la app JobvancyBoard en heroku utilizando terraform para aprovisionar la infra.

## Dependencias 

Los requisitos previos para poder hacer el despliegue son:

- **Git**: Herramienta de versionado.
- **Git-Crypt**: Herramienta elegida para encriptar/desencriptar informacion privilegiada.
- **Terraform**: Herramienta para gestionar la infraestructura.
- **Heroku CLI**: Herramienta para gestionar heroku.

Una vez que este todo ello correctamente instalado pasaremos a los pasos del despliegue.

## Instrucciones para despliegue

Sigue los siguientes pasos para desplegar la aplicación JobVacancy en Heroku:

### 1. Clonar el Repo

```bash
git clone https://gitlab.com/msfs_junin/jobvacancy.git

### 2. Ejecutar script de deploy

En el archivo deploy.sh tengo los pasos necesarios para configurar y desplegar la app.

```bash
./deploy.sh

### 3. Desaprovisionar los recursos

Ejecuto el comando terraform destroy -auto-approve dentro de la carpeta infra, para liberar todos los recursos

```bash
cd infra
terraform destroy -auto-approve


