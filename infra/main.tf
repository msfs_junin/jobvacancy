# Configura el proveedor de Heroku
provider "heroku" {
  # Configuro acceso a heroku
  email = "msfs_junin@yahoo.com.ar"
  api_key = "${chomp(file("../heroku.key"))}"
}

# Defino la aplicación web en Heroku
resource "heroku_app" "jobvacancy_app" {
  name   = "jobvacancy-app"  
  region = "us"              
}

#Agrego postgres
resource "heroku_addon" "postgres" {
  app_id  = heroku_app.jobvacancy_app.id
  plan = "heroku-postgresql:mini"
}

